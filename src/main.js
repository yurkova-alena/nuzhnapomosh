import { createApp } from 'vue'
import App from './App.vue'
import getBranch from './plugins/getBranch'
import store from './store'
import './assets/scss/app.scss'

const app = createApp(App)

app.use(getBranch)

app.use(store)

app.mount('#app')