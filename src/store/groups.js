export default {
    namespaced: true,
    
    state: {
        groups : [
            {
                id: 1,
                name: "Дети",
                groups: [
                    {
                        id: 2,
                        name: "Имеющие редкие заболевания",
                        groups: [
                            {
                                id: 3,
                                name: "Spina Bifida",
                                groups: []
                            },
                            {
                                id: 4,
                                name: "Буллёзный эпидермолиз",
                                groups: []
                            }
                        ]
                    },
                    {
                        id: 5,
                        name: "С инвалидностью",
                        groups: []
                    }
                ]
            },
            {
                id: 6,
                name: "Профессиональные сообщества",
                groups: []
            },
        ],
        selectedGroupsIds: [],
    },

    mutations: {
        PUSH_SELECTED_ID(state, id) {
            state.selectedGroupsIds.push(id);
        },
        DELETE_SELECTED_ID(state, id) {
            state.selectedGroupsIds = state.selectedGroupsIds.filter((item) => {
                return item !== id;
            });
        },
        CHANGE_SELECTED_ID(state, [prevId, newId]) {
            const index = state.selectedGroupsIds.indexOf(prevId);
            state.selectedGroupsIds[index] = newId;
        },
    },

    actions: {
        addSelectedGroup({commit}, group) {
            commit('PUSH_SELECTED_ID', group.id);
        },

        deleteSelectedGroup({commit}, group) {
            commit('DELETE_SELECTED_ID', group.id);
        },

        changeSelectedGroup({state, commit}, [prevGroup, newGroup]) {
            const hasId = state.selectedGroupsIds.includes(newGroup.id);
            hasId ? commit('DELETE_SELECTED_ID', prevGroup.id)
                : commit('CHANGE_SELECTED_ID', [prevGroup.id, newGroup.id]);
        },
    },
}