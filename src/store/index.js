import { createStore } from 'vuex'
import groups from "@/store/groups";

export default createStore({
  state: {},

  mutations: {},

  actions: {},

  modules: {groups}
})
