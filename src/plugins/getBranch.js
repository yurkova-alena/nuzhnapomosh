function $getBranch(arr, id) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].id === id) return [arr[i]];
        let result = $getBranch(arr[i].groups, id);
        if (result != null) {
            result.unshift(arr[i]);
            return result;
        }
    }
}

export default {
    install: (app) => {
        const getBranch = $getBranch
        app.provide("getBranch", getBranch);
    },
}